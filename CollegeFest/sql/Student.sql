USE `web_customer_tracker`;
select * from customer;

CREATE DATABASE  IF NOT EXISTS student_spring;


USE student_spring;

CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `department` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `student` VALUES 
	(1,'Suresh','B.tech','India'),
	(2,'Muri','B.Arch','Canada'),
	(3,'Daniel','B.tech','New Zealand'),
	(4,'Tanya','B.Com','USA');
    
    select * from Student;