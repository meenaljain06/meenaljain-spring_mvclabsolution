package com.great.learning.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.great.learning.student.model.Student;
import com.great.learning.student.service.StudentService;

@Controller
@RequestMapping("/student")
public class StudentController {
	@Autowired
	private StudentService studentService;


	@RequestMapping("/get")
	public String listBooks(Model theModel) {

		System.out.println("request recieved");
		// get Books from db
		List<Student> students = studentService.findAll();
		

		// add to the spring model
		theModel.addAttribute("Students", students);

		return "list-Students";
	}

	@RequestMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {

		// create model attribute to bind form data
		Student student = new Student();

		theModel.addAttribute("Student", student);

		return "Student-form";
	}

	@RequestMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("studentId") int id,
			Model theModel) {

		// get the Book from the service
		Student student = studentService.findById(id);


		// set Book as a model attribute to pre-populate the form
		theModel.addAttribute("Student", student);

		// send over to our form
		return "Student-form";			
	}


	@PostMapping("/save")
	public String saveBook(@RequestParam("id") int id,
			@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,@RequestParam("course") String course,@RequestParam("country") String country) {

		System.out.println(id);
		Student student;
		if(id!=0)
		{
			student=studentService.findById(id);
			student.setFirstName(firstName);
			student.setLastName(lastName);
			student.setCourse(course);
			student.setCountry(country);
		}
		else
			student=new Student(firstName, lastName, course,country);
		// save the Book
		studentService.save(student);


		// use a redirect to prevent duplicate submissions
		return "redirect:/student/list";

	}


	@RequestMapping("/delete")
	public String delete(@RequestParam("studentId") int id) {

		// delete the Book
		studentService.deleteById(id);

		// redirect to /Books/list
		return "redirect:/student/list";

	}
}
