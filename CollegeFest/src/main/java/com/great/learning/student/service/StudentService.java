package com.great.learning.student.service;

import java.util.List;

import com.great.learning.student.model.Student;

public interface StudentService {
	public List<Student> findAll();

	public Student findById(int id);

	public void save(Student student);

	public void deleteById(int id);
}
